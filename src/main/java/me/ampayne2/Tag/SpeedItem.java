package me.ampayne2.Tag;

import me.ampayne2.ultimategames.UltimateGames;
import me.ampayne2.ultimategames.arenas.Arena;
import me.ampayne2.ultimategames.games.items.GameItem;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class SpeedItem extends GameItem {
    private final UltimateGames ultimateGames;
    private static final ItemStack ITEM;

    public SpeedItem(UltimateGames ultimateGames) {
        super(ITEM, true);
        this.ultimateGames = ultimateGames;
    }

    @Override
    public boolean click(final Arena arena, PlayerInteractEvent event) {
        final Player player = event.getPlayer();
        player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 100, 2));
        Bukkit.getScheduler().scheduleSyncDelayedTask(ultimateGames, new Runnable() {
            @Override
            public void run() {
                if (arena.hasPlayer(player.getName())) {
                    player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 99999, 1));
                }
            }
        }, 100L);
        return true;
    }

    static {
        ITEM = new ItemStack(Material.FEATHER);
        ItemMeta meta = ITEM.getItemMeta();
        meta.setDisplayName(ChatColor.GOLD + "5s Speed Boost");
        ITEM.setItemMeta(meta);
    }
}
