package me.ampayne2.Tag;

import me.ampayne2.ultimategames.UltimateGames;
import me.ampayne2.ultimategames.api.GamePlugin;
import me.ampayne2.ultimategames.arenas.Arena;
import me.ampayne2.ultimategames.arenas.ArenaStatus;
import me.ampayne2.ultimategames.arenas.scoreboards.ArenaScoreboard;
import me.ampayne2.ultimategames.arenas.spawnpoints.PlayerSpawnPoint;
import me.ampayne2.ultimategames.effects.GameSound;
import me.ampayne2.ultimategames.effects.ParticleEffect;
import me.ampayne2.ultimategames.games.Game;
import me.ampayne2.ultimategames.games.items.GameItem;
import me.ampayne2.ultimategames.gson.Gson;
import me.ampayne2.ultimategames.utils.UGUtils;
import me.ampayne2.ultimategames.webapi.WebHandler;
import org.bukkit.*;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.*;

public class Tag extends GamePlugin {
    private UltimateGames ultimateGames;
    private Game game;
    private Map<Arena, String> it = new HashMap<Arena, String>();
    private Map<Arena, String> tagger = new HashMap<Arena, String>();
    private Map<Arena, Integer> timers = new HashMap<Arena, Integer>();
    private List<String> itImmunity = new ArrayList<String>();
    private GameItem speedItem, invisibilityItem;
    private static final ItemStack HEAD, CHEST, LEGS, FEET;
    private static final GameSound TAG_SOUND = new GameSound(Sound.IRONGOLEM_HIT, 10, 2);
    private static final Random RANDOM = new Random();

    @Override
    public boolean loadGame(UltimateGames ultimateGames, Game game) {
        this.ultimateGames = ultimateGames;
        this.game = game;

        speedItem = new SpeedItem(ultimateGames);
        invisibilityItem = new InvisibilityItem();
        ultimateGames.getGameItemManager()
                .registerGameItem(game, speedItem)
                .registerGameItem(game, invisibilityItem);

        return true;
    }

    @Override
    public void unloadGame() {
    }

    @Override
    public boolean reloadGame() {
        return true;
    }

    @Override
    public boolean stopGame() {
        return true;
    }

    @Override
    public boolean loadArena(Arena arena) {
        it.put(arena, null);
        tagger.put(arena, null);
        ultimateGames.addAPIHandler("/" + game.getName() + "/" + arena.getName(), new TagWebHandler(arena));
        return true;
    }

    @Override
    public boolean unloadArena(Arena arena) {
        if (it.containsKey(arena)) {
            it.remove(arena);
        }
        if (tagger.containsKey(arena)) {
            tagger.remove(arena);
        }
        if (timers.containsKey(arena)) {
            endTimer(arena);
        }
        return true;
    }

    @Override
    public boolean isStartPossible(Arena arena) {
        return arena.getStatus() == ArenaStatus.OPEN;
    }

    @Override
    public boolean startArena(Arena arena) {
        return true;
    }

    @Override
    public boolean beginArena(Arena arena) {
        ultimateGames.getCountdownManager().createEndingCountdown(arena, ultimateGames.getConfigManager().getGameConfig(game).getConfig().getInt("CustomValues.GameTime"), true);

        ArenaScoreboard scoreBoard = ultimateGames.getScoreboardManager().createArenaScoreboard(arena, game.getName());
        List<String> players = new ArrayList<String>(arena.getPlayers());
        for (String playerName : arena.getPlayers()) {
            Player player = Bukkit.getPlayerExact(playerName);
            scoreBoard.addPlayer(player);
            scoreBoard.setScore(playerName, 0);
            resetInventory(player, arena);
            player.setHealth(20.0);
            player.setFoodLevel(20);
            if (itImmunity.contains(playerName)) {
                players.remove(playerName);
            }
        }

        //If the list is empty, we set back to original
        if (players.size() == 0) {
            players = arena.getPlayers();
        }

        String playerIt = players.get(RANDOM.nextInt(players.size()));
        scoreBoard.setPlayerColor(Bukkit.getPlayerExact(playerIt), ChatColor.DARK_RED);
        it.put(arena, playerIt);
        ultimateGames.getMessageManager().sendGameMessage(arena, game, "startit", playerIt);
        startTimer(arena);
        scoreBoard.setVisible(true);
        return true;
    }

    @Override
    public void endArena(Arena arena) {
        it.put(arena, null);
        tagger.put(arena, null);
        endTimer(arena);
        String lowestScorer = null;
        String secondLowestScorer = null;
        String thirdLowestScorer = null;
        Integer lowestScore = ultimateGames.getConfigManager().getGameConfig(game).getConfig().getInt("CustomValues.GameTime");
        Integer secondLowestScore = ultimateGames.getConfigManager().getGameConfig(game).getConfig().getInt("CustomValues.GameTime");
        Integer thirdLowestScore = ultimateGames.getConfigManager().getGameConfig(game).getConfig().getInt("CustomValues.GameTime");
        List<String> players = arena.getPlayers();
        ArenaScoreboard scoreBoard = ultimateGames.getScoreboardManager().getArenaScoreboard(arena);
        if (scoreBoard != null) {
            for (String playerName : players) {
                Integer playerScore = scoreBoard.getScore(playerName);
                if (playerScore < lowestScore) {
                    lowestScorer = playerName;
                    lowestScore = playerScore;
                }
            }
            for (String playerName : players) {
                Integer playerScore = scoreBoard.getScore(playerName);
                if (playerScore < secondLowestScore && !playerName.equals(lowestScorer)) {
                    secondLowestScorer = playerName;
                    secondLowestScore = playerScore;
                }
            }
            for (String playerName : players) {
                Integer playerScore = scoreBoard.getScore(playerName);
                if (playerScore < thirdLowestScore && !playerName.equals(lowestScorer) && !playerName.equals(secondLowestScorer)) {
                    thirdLowestScorer = playerName;
                    thirdLowestScore = playerScore;
                }
            }
        }
        if (lowestScorer != null) {
            ultimateGames.getMessageManager().sendGameMessage(ultimateGames.getServer(), game, "GameEnd", lowestScorer, game.getName(), arena.getName());
            ultimateGames.getPointManager().addPoint(game, lowestScorer, "store", 30);
            ultimateGames.getPointManager().addPoint(game, lowestScorer, "first", 1);
            if (secondLowestScorer != null) {
                ultimateGames.getPointManager().addPoint(game, secondLowestScorer, "store", 20);
                ultimateGames.getPointManager().addPoint(game, secondLowestScorer, "second", 1);
            }
            if (thirdLowestScorer != null) {
                ultimateGames.getPointManager().addPoint(game, thirdLowestScorer, "store", 10);
                ultimateGames.getPointManager().addPoint(game, thirdLowestScorer, "third", 1);
            }
        }
    }

    @Override
    public boolean resetArena(Arena arena) {
        return true;
    }

    @Override
    public boolean openArena(Arena arena) {
        return true;
    }

    @Override
    public boolean stopArena(Arena arena) {
        return true;
    }

    @Override
    public boolean addPlayer(Player player, Arena arena) {
        if (arena.getStatus() == ArenaStatus.OPEN && arena.getPlayers().size() >= arena.getMinPlayers() && !ultimateGames.getCountdownManager().hasStartingCountdown(arena)) {
            ultimateGames.getCountdownManager().createStartingCountdown(arena, ultimateGames.getConfigManager().getGameConfig(game).getConfig().getInt("CustomValues.StartWaitTime"));
        }
        PlayerSpawnPoint spawnPoint = ultimateGames.getSpawnpointManager().getRandomSpawnPoint(arena);
        spawnPoint.lock(false);
        spawnPoint.teleportPlayer(player);
        for (PotionEffect potionEffect : player.getActivePotionEffects()) {
            player.removePotionEffect(potionEffect.getType());
        }
        player.getInventory().clear();
        player.getInventory().setArmorContents(null);
        player.setHealth(20.0);
        player.setFoodLevel(20);

        if (ultimateGames.getPointManager().hasPerk(game, player.getName(), "itimmunity")) {
            itImmunity.add(player.getName());
        }

        return true;
    }

    @Override
    public void removePlayer(Player player, Arena arena) {
        if (arena.getStatus() == ArenaStatus.RUNNING) {
            List<String> players = arena.getPlayers();
            String playerName = player.getName();
            if (players.size() < 3) {
                ultimateGames.getArenaManager().endArena(arena);
            } else if (it.get(arena) != null && it.get(arena).equals(playerName)) {
                String playerIt = arena.getPlayers().get(RANDOM.nextInt(arena.getPlayers().size()));
                it.put(arena, playerIt);
                ArenaScoreboard scoreBoard = ultimateGames.getScoreboardManager().getArenaScoreboard(arena);
                if (tagger.containsKey(arena)) {
                    if (scoreBoard != null) {
                        scoreBoard.resetPlayerColor(tagger.get(arena));
                    }
                    tagger.put(arena, null);
                }
                if (scoreBoard != null) {
                    scoreBoard.resetPlayerColor(playerName);
                }
                ultimateGames.getMessageManager().sendGameMessage(arena, game, "startit", playerIt);
                ultimateGames.getMessageManager().sendGameMessage(Bukkit.getPlayerExact(playerIt), game, "becomeit");
            } else if (tagger.get(arena) != null && tagger.get(arena).equals(playerName)) {
                ArenaScoreboard scoreBoard = ultimateGames.getScoreboardManager().getArenaScoreboard(arena);
                if (scoreBoard != null) {
                    scoreBoard.resetPlayerColor(playerName);
                }
                tagger.put(arena, null);
            }
        } else if (arena.getStatus() == ArenaStatus.STARTING && arena.getPlayers().size() < arena.getMinPlayers() && ultimateGames.getCountdownManager().hasStartingCountdown(arena)) {
            ultimateGames.getCountdownManager().stopStartingCountdown(arena);
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    public boolean addSpectator(Player player, Arena arena) {
        ultimateGames.getSpawnpointManager().getSpectatorSpawnPoint(arena).teleportPlayer(player);
        for (PotionEffect potionEffect : player.getActivePotionEffects()) {
            player.removePotionEffect(potionEffect.getType());
        }
        player.setHealth(20.0);
        player.setFoodLevel(20);
        player.getInventory().clear();
        player.getInventory().addItem(UGUtils.createInstructionBook(game));
        player.getInventory().setArmorContents(null);
        player.updateInventory();
        return true;
    }

    @Override
    public void removeSpectator(Player player, Arena arena) {
    }

    @Override
    public void onPlayerDeath(Arena arena, PlayerDeathEvent event) {
        event.getDrops().clear();
        UGUtils.autoRespawn(event.getEntity());
    }

    @Override
    public void onPlayerRespawn(Arena arena, PlayerRespawnEvent event) {
        event.setRespawnLocation(ultimateGames.getSpawnpointManager().getRandomSpawnPoint(arena).getLocation());
        resetInventory(event.getPlayer(), arena);
    }

    @Override
    public void onEntityDamage(Arena arena, EntityDamageEvent event) {
        if (arena.getStatus() != ArenaStatus.RUNNING) {
            event.setCancelled(true);
        }
    }

    @Override
    public void onEntityDamageByEntity(Arena arena, EntityDamageByEntityEvent event) {
        if (event.getEntity() instanceof Player) {
            Entity damager = event.getDamager();
            if (damager instanceof Player) {
                Player playerTagged = (Player) event.getEntity();
                Player playerWhoTagged = (Player) damager;
                if (it.get(arena) != null && it.get(arena).equals(playerWhoTagged.getName())) {
                    if (tagger.get(arena) == null || !tagger.get(arena).equals(playerTagged.getName())) {
                        ArenaScoreboard scoreBoard = ultimateGames.getScoreboardManager().getArenaScoreboard(arena);
                        if (scoreBoard != null) {
                            it.put(arena, playerTagged.getName());
                            if (tagger.get(arena) != null) {
                                scoreBoard.resetPlayerColor(tagger.get(arena));
                            }
                            tagger.put(arena, playerWhoTagged.getName());
                            scoreBoard.setPlayerColor(playerTagged, ChatColor.DARK_RED);
                            scoreBoard.setPlayerColor(playerWhoTagged, ChatColor.GREEN);
                            ultimateGames.getMessageManager().sendGameMessage(arena, game, "tag", playerWhoTagged.getName(), playerTagged.getName());
                            ultimateGames.getMessageManager().sendGameMessage(playerTagged, game, "becomeit");
                            UGUtils.spawnFirework(playerTagged.getLocation(), true);
                            TAG_SOUND.play(playerTagged.getEyeLocation());
                        }
                    } else {
                        ultimateGames.getMessageManager().sendGameMessage(playerWhoTagged, game, "Notagbacks");
                        ParticleEffect.HEART.display(playerTagged.getEyeLocation(), 1, 1, 1, 2, 50);
                    }
                }
                event.setDamage(0);
            }
        }
    }

    @Override
    public void onPlayerFoodLevelChange(Arena arena, FoodLevelChangeEvent event) {
        event.setCancelled(true);
    }

    @Override
    public void onItemPickup(Arena arena, PlayerPickupItemEvent event) {
        event.setCancelled(true);
    }

    @Override
    public void onItemDrop(Arena arena, PlayerDropItemEvent event) {
        event.setCancelled(true);
    }

    @SuppressWarnings("deprecation")
    private void resetInventory(final Player player, final Arena arena) {
        player.getInventory().clear();
        player.getInventory().setArmorContents(null);
        player.getInventory().addItem(UGUtils.createInstructionBook(game));
        Bukkit.getScheduler().scheduleSyncDelayedTask(ultimateGames, new Runnable() {
            @Override
            public void run() {
                if (arena.hasPlayer(player.getName())) {
                    player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 99999, 1));
                }
            }
        }, 20L);

        if (ultimateGames.getPointManager().hasPerk(game, player.getName(), "blackarmor")) {
            player.getInventory().setHelmet(HEAD);
            player.getInventory().setChestplate(CHEST);
            player.getInventory().setLeggings(LEGS);
            player.getInventory().setBoots(FEET);
        }

        if (ultimateGames.getPointManager().hasPerk(game, player.getName(), "speedpotion")) {
            player.getInventory().addItem(speedItem.getItem());
        }

        if (ultimateGames.getPointManager().hasPerk(game, player.getName(), "invisibility")) {
            player.getInventory().addItem(invisibilityItem.getItem());
        }

        player.updateInventory();
    }

    public void startTimer(final Arena arena) {
        timers.put(arena, Bukkit.getScheduler().scheduleSyncRepeatingTask(ultimateGames, new Runnable() {
            @Override
            public void run() {
                if (it.containsKey(arena)) {
                    ArenaScoreboard scoreBoard = ultimateGames.getScoreboardManager().getArenaScoreboard(arena);
                    if (scoreBoard != null) {
                        scoreBoard.setScore(it.get(arena), scoreBoard.getScore(it.get(arena)) + 1);
                    }
                } else {
                    endTimer(arena);
                }
            }
        }, 20L, 20L));
    }

    public void endTimer(Arena arena) {
        if (timers.containsKey(arena)) {
            Bukkit.getScheduler().cancelTask(timers.get(arena));
            timers.remove(arena);
        }
    }

    static {
        HEAD = new ItemStack(Material.LEATHER_HELMET, 1);
        LeatherArmorMeta meta = (LeatherArmorMeta) HEAD.getItemMeta();
        meta.setColor(Color.fromRGB(0, 0, 0));
        HEAD.setItemMeta(meta);

        CHEST = new ItemStack(Material.LEATHER_CHESTPLATE, 1);
        meta = (LeatherArmorMeta) CHEST.getItemMeta();
        meta.setColor(Color.fromRGB(0, 0, 0));
        CHEST.setItemMeta(meta);

        LEGS = new ItemStack(Material.LEATHER_LEGGINGS, 1);
        meta = (LeatherArmorMeta) LEGS.getItemMeta();
        meta.setColor(Color.fromRGB(0, 0, 0));
        LEGS.setItemMeta(meta);

        FEET = new ItemStack(Material.LEATHER_BOOTS, 1);
        meta = (LeatherArmorMeta) FEET.getItemMeta();
        meta.setColor(Color.fromRGB(0, 0, 0));
        FEET.setItemMeta(meta);
    }

    private class TagWebHandler implements WebHandler {
        private Arena arena;

        public TagWebHandler(Arena arena) {
            this.arena = arena;
        }

        @Override
        public String sendResult() {
            Gson gson = new Gson();
            Map<String, Integer> playerList = new HashMap<String, Integer>();

            ArenaScoreboard scoreBoard = ultimateGames.getScoreboardManager().getArenaScoreboard(arena);
            if (scoreBoard != null) {
                for (String playerName : arena.getPlayers()) {
                    playerList.put(playerName, scoreBoard.getScore(playerName));
                }
            }

            return gson.toJson(playerList);
        }
    }
}
