package me.ampayne2.Tag;

import me.ampayne2.ultimategames.arenas.Arena;
import me.ampayne2.ultimategames.games.items.GameItem;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class InvisibilityItem extends GameItem {
    private static final ItemStack ITEM;

    public InvisibilityItem() {
        super(ITEM, true);
    }

    @Override
    public boolean click(final Arena arena, PlayerInteractEvent event) {
        event.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 200, 1));
        return true;
    }

    static {
        ITEM = new ItemStack(Material.GLASS);
        ItemMeta meta = ITEM.getItemMeta();
        meta.setDisplayName(ChatColor.GOLD + "10s Invisibility");
        ITEM.setItemMeta(meta);
    }
}
